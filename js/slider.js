(function($, drupalSettings) {

    Drupal.behaviors.bannerSlider = {
        attach: function(context, settings) {
            var sliders = settings.bannerSlider || {};

            $.each(sliders, function(k, v) {
                if (!v.initialized) {
                    $('[data-slick="' + k + '"]', context).slick($.extend(v.settings, {
                        autoplay: true,
                        autoplaySpeed: 2000,
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 2
                                }
                            },
                            {
                                breakpoint: 450,
                                settings: {
                                    slidesToShow: 1
                                }
                            }
                        ]
                    }));
                    v.initialized = true;
                }
            });
        }
    };

})(jQuery, drupalSettings);