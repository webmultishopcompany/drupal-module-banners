<?php
/**
 * @file
 * Contains \Drupal\wmc_banners\Plugin\Block\BannerBlock.
 */

namespace Drupal\wmc_banners\Plugin\Block;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @Block(
 *   id = "wmc_banners",
 *   admin_label = @Translation("Banners"),
 *   category = @Translation("Straw"),
 * )
 */
class WmcBannerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ElementInfoManagerInterface $element_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->elementInfo = $element_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('element_info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'banners' => [],
      'theme' => 'banners',
      'image_style' => NULL,
      'banners_to_show' => 4,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = [
      '#theme' => $config['theme'],
      '#banners_to_show' => $config['banners_to_show'],
    ];

    $banners = $config['banners'];

    $file_storage = \Drupal::entityTypeManager()->getStorage('file');

    foreach ($banners as $index => $banner) {
      $file = $file_storage->load($banner['image']);
      if (!is_null($file)) {
        $file_uri = $file->getFileUri();

        $image = \Drupal::service('image.factory')->get($file_uri);
        if ($image->isValid()) {
          $width = $image->getWidth();
          $height = $image->getHeight();
        }
        else {
          $width = $height = NULL;
        }

        $build['#banners'][$index]['image'] = [
          'image' => [
            '#theme' => 'image_style',
            '#width' => $width,
            '#height' => $height,
            '#style_name' => $config['image_style'],
            '#uri' => $file_uri,
            '#alt' => $banner['alt'],
          ],
        ];

        $build['#banners'][$index]['url'] = $banner['url'];
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $max_file_size = Bytes::toInt('3MB');
    $max_php_file_size = file_upload_max_size();
    $managed_file_info = $this->elementInfo->getInfo('managed_file');

    $banner_count = $form_state->get('banner_count');
    if (is_null($banner_count)) {
      if (isset($config['banners'])) {
        $banner_count = count($config['banners']);
      }
      if ($banner_count == 0) {
        $banner_count = 1;
      }
      $form_state->set('banner_count', $banner_count);
    }

    $form['theme'] = [
      '#title' => $this->t('Theme'),
      '#type' => 'select',
      '#default_value' => $config['theme'],
      '#options' => [
        'banners' => $this->t('Default'),
        'banner_slider' => $this->t('Slider'),
      ],
    ];

    $form['banners_to_show'] = [
      '#title' => $this->t('Banners to show'),
      '#type' => 'number',
      '#default_value' => $config['banners_to_show'],
    ];

    $image_styles = image_style_options(FALSE);
    $form['image_style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $config['image_style'],
      '#options' => $image_styles,
    ];

    $form['banners'] = [
      '#tree' => TRUE,
      '#type' => 'table',
      '#prefix' => '<div id="banners-wrapper">',
      '#suffix' => '</div>',
      '#header' => [
        'label' => '',
        'weight' => $this->t('Weight'),
        'image' => $this->t('Image') . '*',
        'alt' => $this->t('Alt') . '*',
        'url' => $this->t('Url'),
        'operations' => $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $form['add_banner'] = [
      '#type' => 'actions',
      'add_banner' => [
        '#type' => 'submit',
        '#value' => t('Add banner'),
        '#submit' => [get_class($this) . '::addBanner'],
        '#ajax' => [
          'callback' => get_class($this) . '::bannerAjaxCallback',
          'wrapper' => 'banners-wrapper',
        ],
      ],
    ];

    $banners_deleted = $form_state->get('banners_deleted');
    if (is_null($banners_deleted)) {
      $banners_deleted = [];
    }

    for ($i = 0; $i < $banner_count; $i ++) {
      if (in_array($i, $banners_deleted) == TRUE) {
        continue;
      }

      $form['banners'][$i]['#attributes']['class'][] = 'draggable';

      $form['banners'][$i]['#weight'] = isset($config['banners'][$i]['#weight']) ? $config['banners'][$i]['#weight'] : 1;

      // Needed for dabledrag to work.
      $form['banners'][$i]['label'] = [
        '#markup' => '',
      ];

      $form['banners'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => isset($config['banners'][$i]['#weight']) ? $config['banners'][$i]['#weight'] : 1,
        '#attributes' => ['class' => ['weight']],
      ];

      $form['banners'][$i]['image']['fid'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Image'),
        '#title_display' => 'invisible',
        '#default_value' => isset($config['banners'][$i]['image']) ? [$config['banners'][$i]['image']] : NULL,
        '#required' => TRUE,
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg'],
          'file_validate_size' => [min($max_file_size, $max_php_file_size)],
        ],
        '#process' => array_merge($managed_file_info['#process'], [[get_class($this), 'processImage']]),
        '#upload_location' => 'public://carousel',
      ];

      $form['banners'][$i]['image']['help'] = [
        '#type' => 'container',
        'text' => [
          '#theme' => 'file_upload_help',
          '#weight' => 20,
          '#description' => '',
          '#upload_validators' => $form['banners'][$i]['image']['fid']['#upload_validators'],
        ],
      ];

      $form['banners'][$i]['alt'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Alt'),
        '#title_display' => 'invisible',
        '#size' => 20,
        '#required' => TRUE,
        '#default_value' => isset($config['banners'][$i]['alt']) ? $config['banners'][$i]['alt'] : NULL,
      ];

      $form['banners'][$i]['url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Url'),
        '#title_display' => 'invisible',
        '#default_value' => isset($config['banners'][$i]['url']) ? $config['banners'][$i]['url'] : NULL,
      ];

      $form['banners'][$i]['operations'] = [];

      if ($banner_count - count($banners_deleted) > 1) {
        $form['banners'][$i]['operations']['remove'] = [
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'remove_banner_' . $i,
          '#submit' => [get_class($this) . '::removeBanner'],
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => get_class($this) . '::bannerAjaxCallback',
            'wrapper' => 'banners-wrapper',
          ],
          '#attributes' => [
            'banner_key' => $i,
          ],
        ];
      }
    }

    $form_state->setCached(FALSE);

    return $form;
  }

  /**
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $form
   *
   * @return mixed
   */
  public static function processImage($element, FormStateInterface $form_state, $form) {
    $item = $element['#value'];
    $item['fids'] = $element['fids']['#value'];

    // Add the image preview.
    if (!empty($element['#files'])) {
      $file = reset($element['#files']);

      $variables = [
        'style_name' => 'thumbnail',
        'uri' => $file->getFileUri(),
      ];

      // Determine image dimensions.
      if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
        $variables['width'] = $element['#value']['width'];
        $variables['height'] = $element['#value']['height'];
      }
      else {
        $image = \Drupal::service('image.factory')->get($file->getFileUri());
        if ($image->isValid()) {
          $variables['width'] = $image->getWidth();
          $variables['height'] = $image->getHeight();
        }
        else {
          $variables['width'] = $variables['height'] = NULL;
        }
      }

      $element['preview'] = [
        '#type' => 'container',
        '#attributes' => [
          // @todo Remove inline style.
          'style' => 'float: left; margin-right: 10px; min-width: 100px; min-height: 100px',
        ],
        'image' => [
          '#weight' => -10,
          '#theme' => 'image_style',
          '#width' => $variables['width'],
          '#height' => $variables['height'],
          '#style_name' => $variables['style_name'],
          '#uri' => $variables['uri'],
        ],
      ];
    }

    return $element;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function bannerAjaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['banners'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addBanner(array &$form, FormStateInterface $form_state) {
    $banner_count = $form_state->get('banner_count');
    $form_state->set('banner_count', $banner_count + 1);
    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function removeBanner(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#attributes']['banner_key'])) {
      $banner_key = $triggering_element['#attributes']['banner_key'];

      $banners_deleted = $form_state->get('banners_deleted');
      if (is_null($banners_deleted)) {
        $banners_deleted = [];
      }

      $banners_deleted[] = $banner_key;
      $form_state->set('banners_deleted', $banners_deleted);

      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $banners_deleted = $form_state->get('banners_deleted');
    if (is_null($banners_deleted)) {
      $banners_deleted = [];
    }
    
    foreach ($form_state->getValues() as $key => $value) {
      if ($key != 'banners') {
        $this->setConfigurationValue($key, $value);
      }
      else {
        $banners_to_save = [];

        foreach ($value as $banner_key => $banner_value) {
          if ($banner_key !== 'actions' && $banner_key !== 'add_banner') {
            if (in_array($banner_key, $banners_deleted) === FALSE) {
              $banner_to_save = [
                'weight' => $banner_value['weight'],
                'alt' => $banner_value['alt'],
                'url' => $banner_value['url'],
              ];

              if ($this->saveFile(reset($banner_value['image']['fid']), 'banners', 'settings')) {
                $banner_to_save['image'] = reset($banner_value['image']['fid']);
              }

              $banners_to_save[] = $banner_to_save;
            }
          }
        }

        $config = $this->getConfiguration();
        foreach ($banners_deleted as $i) {
          if (isset($config['banners'][$i]['image'])) {
            $this->deleteFile($config['banners'][$i]['image'], 'banners', 'settings');
          }
        }

        $this->setConfigurationValue($key, $banners_to_save);
      }
    }
  }

  /**
   * @param $fid
   * @param $module_name
   * @param $file_type
   *
   * @return bool
   */
  protected function saveFile($fid, $module_name, $file_type) {
    /** @var File $file */
    $file = File::load($fid);
    if (!is_null($fid)) {
      if ($file->isTemporary()) {
        /** @var DatabaseFileUsageBackend $file_usage */
        $file_usage = \Drupal::service('file.usage');
        $file_usage->add($file, $module_name, $file_type, 1);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param $fid
   * @param $module_name
   * @param $file_type
   *
   * @return bool
   */
  protected function deleteFile($fid, $module_name, $file_type) {
    /** @var File $file */
    $file = File::load($fid);
    if (!is_null($fid)) {
      /** @var DatabaseFileUsageBackend $file_usage */
      $file_usage = \Drupal::service('file.usage');
      $file_usage->delete($file, $module_name, $file_type, 1);
      return TRUE;
    }
    return FALSE;
  }
}
